#!/bin/bash
start=$(date +%s.%N)

sleep 1
end=$(date +%s.%N)    
runtime=$(echo "$end $start" | awk '{printf $1 - $2}' | cut -b 1-5)

echo "Runtime was $runtime"
