#!/bin/bash


chosen=$(echo -e "Poweroff\nReboot\nSuspend\nHibernate" | rofi -dmenu -i)

if [[ $chosen = "Poweroff" ]]; then
	systemctl poweroff
elif [[ $chosen = "Reboot" ]]; then
	systemctl reboot
elif [[ $chosen = "Suspend" ]]; then
	systemctl suspend
elif [[ $chosen = "Hibernate" ]]; then
	systemctl hibernate
fi
