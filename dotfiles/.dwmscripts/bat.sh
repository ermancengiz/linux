#!/bin/bash

volt=$(echo "scale=2;($(cat /sys/class/power_supply/BAT0/voltage_now) / 1000000)" | bc)
amper=$(echo - | awk "{printf \"%.1f\", $(( $(cat /sys/class/power_supply/BAT0/current_now) *   $(cat /sys/class/power_supply/BAT0/voltage_now) )) / 10000000000000 }")
watt=$(echo - | awk "{printf \"%.1f\", $((   $(cat /sys/class/power_supply/BAT0/current_now) *   $(cat /sys/class/power_supply/BAT0/voltage_now) )) / 1000000000000 }")


charge=$(acpi | awk '{print  $3}' | cut -c -4)
oran=$(acpi | awk '{print  $4 }' | cut -c -3 )
sure=$(acpi | awk '{print  $5 }')

   echo "$watt W $sure $oran" 



