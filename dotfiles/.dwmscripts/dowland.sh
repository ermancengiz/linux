#!/bin/bash
  
INTERFACE=$(ip route | awk '/^default/ { print $5 }' | head -n 1 )

#echo -ne $INTERFACE " "
#echo -ne $(date +%H:%m:%S ) " " $INTERFACE "  \n" >> ~/veri

if [ -z $INTERFACE ] 
then
    echo -ne "  0.0 "
    exit
else

	path="/dev/shm/$(basename $0)-${INTERFACE}"

	read rx < "/sys/class/net/${INTERFACE}/statistics/rx_bytes"
	read tx < "/sys/class/net/${INTERFACE}/statistics/tx_bytes"

	time=$(date +%s)
	if ! [[ -f "${path}" ]]; then
  		echo "${time} ${rx} ${tx}" > "${path}"
  		chmod 0666 "${path}"
	fi

	read old < "${path}"
	echo "${time} ${rx} ${tx}" > "${path}"

	old=(${old//;/ })

	time_diff=$(( $time - ${old[0]} ))

	[[ "${time_diff}" -gt 0 ]] || exit

	rx_diff=$(( $rx - ${old[1]} ))
	tx_diff=$(( $tx - ${old[2]} ))
	rx_rate=$(( $rx_diff / $time_diff ))
	tx_rate=$(( $tx_diff / $time_diff ))

	dowland=$(( $rx_rate / 1024 ))
	upluad=$(( $tx_rate  / 1024))

 	
	printf %.1f "$(( $dowland ))e-3" 
	echo -ne " "

  printf %.1f "$(( $dowland ))e-3" > ~/.data/dowland
##echo -ne " U " 
a	##printf %.1f "$(( $upluad ))e-3"

fi
