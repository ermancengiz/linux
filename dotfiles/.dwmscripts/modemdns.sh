num=$(xset q | grep "Num" | awk '{printf $8}')
caps=$(xset q | grep "Num" | awk '{printf $4 }')

if [ $num == "on" ]
then
    printf "N ●" 
elif [ $num == "off" ]
then
    printf "N ○" 
else
    echo ""
fi


if [ $caps == "on" ]
then
    printf "  C ●" 
elif [ $caps == "off" ]
then
    printf "  C ○" 
else
    echo ""
fi
