#!/bin/bash

writetmp=`awk '/sda / {print $10  }' /proc/diskstats`
readtmp=`awk '/sda / {print $6 }' /proc/diskstats`

write=`echo "$writetmp / 2" | bc`
read=`echo "$readtmp / 2"  | bc`



path="/dev/shm/diskread"




time=$(date +%s)


if ! [[ -f "${path}" ]]; then
  echo "${time} ${read} ${write}" > "${path}"
  chmod 0666 "${path}"
fi


read old < "${path}"
echo "${time} ${read} ${write}" > "${path}"


old=(${old//;/ })
time_diff=$(( $time - ${old[0]} ))


[[ "${time_diff}" -gt 0 ]] || exit






rx_diff=$(( $read - ${old[1]} ))
wx_diff=$(( $write - ${old[2]} ))
rx_rate=$(( $rx_diff / $time_diff ))
tx_rate=$(( $wx_diff / $time_diff ))


readmega=$(( $rx_rate / 1024  ))

writemega=$(( $tx_rate / 1024 ))




echo -ne "  "
    printf "%03.0f" $readmega
echo -ne " "


printf "%03.0f" $readmega > ~/.data/readdisk
echo -ne " "
