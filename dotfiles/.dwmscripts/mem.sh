#/bin/bash

usageram=$( echo " $(free -m --si | grep 'Mem:' | awk  '{print $3  }' ) " )
freeram=$(  echo " $(free -m --si | grep 'Mem:' | awk  '{printf $4 }' ) " )
usageswap=$(echo " $(free -m --si | grep 'Swap:'| awk  '{printf $3 }' ) " )


echo -ne "  "

printf %.1f "$(( $usageram  ))e-3" 
printf "   "
printf %.1f "$(( $freeram   ))e-3" 
printf "   "
printf %.1f "$(( $usageswap ))e-3" 
printf " "



echo $usageram  > ~/.data/usageram
echo $freeram   > ~/.data/freeram
echo $usageswap > ~/.data/usageswap
