swap=$(echo "[$(free -m --si | awk '/^Swap:/ {print ($3)/1024  }' |cut -b 1-3 )]")
if [ "$swap" == "[0]" ]; 
then
    echo -ne "[0.0]"
else
    echo -ne $swap
fi


