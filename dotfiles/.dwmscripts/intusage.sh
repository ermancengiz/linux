wifidown=$(cat /sys/class/net/wlp3s0f0/statistics/rx_bytes  )
wifiup=$(cat /sys/class/net/wlp3s0f0/statistics/tx_bytes)

ethdown=$(cat /sys/class/net/enp4s0/statistics/rx_bytes )
ethup=$(cat /sys/class/net/enp4s0/statistics/tx_bytes  )

downbyte=$(echo $wifidown $ethdown |awk '{print $1 + $2 }' ) 
upbyte=$(echo $wifiup $ethup |awk '{print $1 + $2 }'  )

downgb=$(echo $downbyte | awk '{print $0 / 1073741824  } ' | cut -b 1-4 )
upgb=$(echo $upbyte | awk '{print $0 / 1073741824 }' | cut -b 1-4 )

if [ $downbyte -gt 108003328 ]
then

    printf "D $downgb" 
    printf "GB "

else
    printf "D 0.0GB "
fi 
 


if [ $upbyte -gt 108003328 ]
then

    printf "U $upgb" 
    printf "GB "

else
    printf "U 0.0GB "
fi 
 

