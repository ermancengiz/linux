local awful = require("awful")

terminal = "tilix"
--terminal = "urxvt"
editor = os.getenv("EDITOR") or "nvim"
editor_cmd = terminal .. " -e " .. editor

browser  = "firefox"
file     = "dolphin --platformtheme kde"


rofirunner  = "rofi -show drun -show-icons -icon-theme MY_ICON_THEME"
dmenurunner = "dmenu_run -nf '#BBBBBB' -nb '#050E15' -sb '#7cb7ff' -sf '#000' -fn 'Consolas-12' -p 'App :'"


--1d1f21
--theme.bg_normal             = "#050E15"


awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.corner.nw,
 --   utils.centerwork,
    awful.layout.suit.floating,
}

awful.util.tagnames = {
	{
		{name = "1", sel = true},
		{name = "2", lay = awful.layout.layouts[3], mw = 0.87},
        {name = "3", sgap = true},
		{name = "4"},
		{name = "5"},
		{name = "6"},
		{name = "7"},
		{name = "8"},
		{name = "9"},
		{name = "10"},
	},
}

