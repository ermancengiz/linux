local os = os


local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local naughty = require("naughty")
local awful = require("awful")

local theme = {}
theme.dir = os.getenv("HOME") .. "/.config/awesome/theme"




-- Fonts
theme.font         = "Roboto Medium 9"
theme.taglist_font = "Roboto Bold 9"
theme.iconFont     = "Font Awesome 5 Free Regular 9"


-- Standard Background and Foreground
--theme.bg_normal  = "#1D1F21"  --theme.xbackground
theme.bg_normal             = "#050E15"
theme.xbackground           = "#161925"
-- Taglist
theme.taglist_bg_focus     = "#7cb7ff" --theme.xcolor4
theme.taglist_fg_focus     = "#000" 

theme.taglist_bg_empty     = theme.bg_normal
theme.taglist_fg_empty     = "#282c34" --theme.xcolor8

theme.taglist_bg_occupied  = theme.bg_normal 
theme.taglist_fg_occupied  = "#7cb7ff" -- theme.xcolor4

theme.taglist_bg_urgent    = theme.bg_normal
theme.taglist_fg_urgent    = "ed254e" --theme.xcolor1








theme.taglist_spacing = dpi(0)



-- Borders
theme.border_width  = dpi(2)
theme.border_normal = theme.xbackground
theme.border_focus  = "#dcdfe4" --theme.xcolor7




theme.awesome_icon      = theme.dir .."/icons/awesome.png"
theme.layout_tile       = theme.dir .. "/icons/tile.png"
theme.layout_tileleft   = theme.dir .. "/icons/tileleft.png"
theme.layout_tilebottom = theme.dir .. "/icons/tilebottom.png"
theme.layout_tiletop    = theme.dir .. "/icons/tiletop.png"
theme.layout_fairv      = theme.dir .. "/icons/fairv.png"
theme.layout_fairh      = theme.dir .. "/icons/fairh.png"
theme.layout_spiral     = theme.dir .. "/icons/spiral.png"
theme.layout_dwindle    = theme.dir .. "/icons/dwindle.png"
theme.layout_fullscreen = theme.dir .. "/icons/fullscreen.png"
theme.layout_magnifier  = theme.dir .. "/icons/magnifier.png"
theme.layout_floating   = theme.dir .. "/icons/floating.png"
theme.layout_cornernw   = theme.dir .. "/icons/cornernw.png"
theme.layout_centerwork = theme.dir .. "/icons/centerwork.png"


theme.useless_gap           = dpi(3) -- 4
theme.screen_margin         = dpi(5) -- 5
theme.maximized_hide_border = true





naughty.config.padding                          = 8
naughty.config.spacing                          = 9
naughty.config.defaults.timeout                 = 10
naughty.config.defaults.fg                      = "#FFF"
naughty.config.defaults.bg                      = "#00000080"
naughty.config.defaults.border_width            = 2
naughty.config.defaults.margin                  = 9

naughty.config.defaults.height                  = 110
naughty.config.defaults.width                   = 450


naughty.config.defaults.position                = "bottom_right"


naughty.config.presets.normal                   
= { 
    bg            = "#00000080",
    fg            = "#FFFFFF",
    border_width  = 1,
    margin        = 12,
    icon_size     = 85,
     
}





return theme
