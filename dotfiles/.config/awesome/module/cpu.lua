
local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")

local cpu_widget = {}

local function worker(args)

    local args = args or {}
    local cpu_s = args.cpu


    --- Main ram widget shown on wibar
    cpu_widget = wibox.widget {
        border_width = 0,
        colors = {  "#7cb7ff" , '#26403f'},
        display_labels = false,
        forced_width = 22,
        widget = wibox.widget.piechart
    }


    watch('bash -c "bash /home/alita/.scripts/cpus.sh '.. cpu_s .. ' "', 1,
        function(widget, stdout, stderr, exitreason, exitcode)

            widget.data = { stdout  , 100 - stdout } 
        end,
        cpu_widget
    )



    return cpu_widget 
end

return setmetatable(cpu_widget , { __call = function(_, ...)
    return worker(...)
end })
