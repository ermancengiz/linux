
local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")

local date_widget = {}

local function worker(args)

    local args = args or {}
    local timeout = args.timeout or 1

    --- Main ram widget shown on wibar
    date_widget = wibox.widget {
        widget = wibox.widget.textbox
      }


    watch('bash -c "  sensors | ' .. ' grep "Package" ' .. " | awk '{ print $4 }'  " ..  ' | cut -c2-3 '  .. ' "', timeout,
        function(widget, stdout, stderr, exitreason, exitcode)
           

          widget:set_text(stdout)

        end,
        date_widget
    )



    return date_widget
end

return setmetatable(date_widget, { __call = function(_, ...)
    return worker(...)
end })
