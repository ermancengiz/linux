# Defined in /home/alita/.config/fish/functions/testtest.fish @ line 2
function twitch.video
	echo $argv[1] $argv[2]


  set res (echo -e "160p\n360p\n480p\n720p\n720p60\n1080p60" | fzf)
 
  if test -z $argv[2]
    streamlink --player "mpv" "$argv[1]" $res
  else
    streamlink --player "mpv" "$argv[1]" $res --hls-start-offset $argv[2] 
  end
end
