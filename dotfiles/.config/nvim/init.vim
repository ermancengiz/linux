set shell=/bin/bash

set rtp+=~/.fzf

if has("nvim")
  set termguicolors
endif 


set nocompatible   
filetype off    

call plug#begin('~/.vim/plugged')
        
    Plug 'dense-analysis/ale'

    Plug 'scrooloose/nerdtree'
    Plug 'Xuyuanp/nerdtree-git-plugin'
    Plug 'jistr/vim-nerdtree-tabs'

    Plug 'ryanoasis/vim-devicons'
    Plug 'ryanoasis/vim-webdevicons'

"   Plug 'ap/vim-css-color'
    Plug 'lilydjwg/colorizer'

    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'mkitt/tabline.vim'

    Plug 'airblade/vim-gitgutter'
    Plug 'tpope/vim-fugitive'


    Plug 'pangloss/vim-javascript'
    Plug 'valloric/youcompleteme'
    Plug 'SirVer/ultisnips'
    Plug 'honza/vim-snippets'

    Plug 'rking/ag.vim'
    Plug 'junegunn/fzf.vim'   
    Plug 'haya14busa/is.vim'

call plug#end()

filetype plugin indent on 

set encoding=utf-8
syntax on

colorscheme ayu
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='transparent'

set number
set numberwidth=3
set cursorline


set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

set fillchars=""

set fillchars=eob:-


let g:ale_linters = {
\   'javascript': ['eslint'],
\}

let b:ale_fixers = ['prettier', 'eslint']
" Equivalent to the above.
let b:ale_fixers = {'javascript': ['prettier', 'eslint']}


let g:ycm_python_binary_path = '/usr/bin/python3'
let g:ycm_seed_identifiers_with_syntax = 1 " Completion for programming language's keyword
let g:ycm_complete_in_comments = 1 " Completion in comments
let g:ycm_complete_in_strings = 1 " Completion in string
let g:ycm_key_list_stop_completion = ['<Enter>']
let g:ycm_collect_identifiers_from_tags_files = 1 " Let YCM read tags from Ctags file
let g:ycm_use_ultisnips_completer = 1 " Default 1, just ensure






let g:ag_prg="ag --column --nogroup --noheading --vimgrep"
let g:ag_working_path_mode="r"

highlight Pmenu ctermfg=250 ctermbg=16

" autocmd FileType python map <buffer> <F7> :call Flake8()<CR>
" autocmd FileType go map <buffer> <F7> :GoBuild <CR>

map qq :q!<Enter>
map qw :wq<Enter>
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>

map <ESC>[1;5C <C-Right>
map <ESC>[1;5D <C-Left>
map <ESC>[1;5A <C-Up>
map <ESC>[1;5B <C-Down>

map <C-c> :NERDTreeTabsToggle<CR>
map <F5> :NERDTreeRefreshRoot<CR>

map <C-h> <C-w>h
map <C-Left> <C-w>h
map <C-l> <C-w>l
map <C-Right> <C-w>l
map <C-j> <C-w>j
map <C-Down> <C-w>j
map <C-k> <C-w>k
map <C-Up> <C-w>k



let g:UltiSnipsExpandTrigger="<C-k>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

nnoremap <C-n> :bn<CR>
nnoremap <C-p> :bp<CR>

"map <C-s><up> :tabr<cr>
"map <C-s><down> :tabl<cr>
"map <C-s><left> :tabp<cr>
"map <C-s><right> :tabn<cr>
