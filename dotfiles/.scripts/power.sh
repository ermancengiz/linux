#!/usr/bin/env bash

# rofi-power
# Use rofi to call systemctl for shutdown, reboot, etc

# 2016 Oliver Kraitschy - http://okraits.de

OPTIONS="Kapat\nYeniden-başlat\nRam-Uykusu\nDisk-Uykusu"

  LAUNCHER="rofi -width 30 -dmenu -i -p rofi-power:"
  USE_LOCKER="false"
  LOCKER="i3lock"

# Show exit wm option if exit command is provided as an argument
if [ ${#1} -gt 0 ]; then
  OPTIONS="Exit window manager\n$OPTIONS"
fi

option=`echo -e $OPTIONS | $LAUNCHER | awk '{print $1}' | tr -d '\r\n'`
if [ ${#option} -gt 0 ]
then
    case $option in
      Exit)
        eval $1
        ;;
      Yeniden-başlat)
        systemctl reboot
        ;;
      Kapat)
        systemctl poweroff
        ;;
      Ram-Uykusu)
        $($USE_LOCKER) && "$LOCKER"; systemctl suspend
        ;;
      Disk-Uykusu)
        $($USE_LOCKER) && "$LOCKER"; systemctl hibernate
        ;;
      *)
        ;;
    esac
fi
